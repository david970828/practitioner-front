var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-with, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});


var requestjson = require('request-json');
var path = require('path');
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/dpardo/collections/movimientos?";
var urlusuariosMlab = "https://api.mlab.com/api/1/databases/dpardo/collections/usuarios?";
var movimientosMLab = requestjson.createClient(urlmovimientosMlab);
var usuariosMlab = requestjson.createClient(urlusuariosMlab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function(req, res) {
  res.send(movimientosJSON);
});

app.get('/usuarios',function(req, res) {
  usuariosMlab.get(urlusuariosMlab + apiKey, function(err, resM, body) {
    if(err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
});

/****************************************
 *         Eliminar Movimiento
 *****************************************/
app.delete('/movimientos/:idMovimiento',function(request, response) {
  console.log('ingreso metodo delete deleteMovement');
  const queryName='q={"numMovimiento":"' + request.params.idMovimiento + '"}&';
  movimientosMLab.get(urlmovimientosMlab + queryName + apiKey, function(err, res, body) {
    var respuesta=body[0];
    if(respuesta != undefined) {
      movimientosMLab.delete(urlmovimientosMlab.split('?')[0].trim() + '/' + respuesta._id.$oid + '?' + apiKey, function (errD, resD, bodyD) {
        if (err)
          console.log(err);
        else
          response.send(body);
      });
    }
  });
});

/****************************************
 *         Crear Movimiento
 *****************************************/
app.post('/movimientos', function (request, response) {
  console.log('ingreso metodo post createMovement');
  console.log(request.body);
  usuariosMlab.post(urlmovimientosMlab + apiKey, request.body, function(err, res, body) {
    if(err)
      console.log(err);
    else
      response.send(body);
  });
});

/****************************************
 *         Registro - Crear Usuario
 *****************************************/
app.post('/usuarios', function (request, response) {
  console.log('ingreso metodo post createUser');
  const queryName = 'q={"documento":"' + request.body.documento + '"}&';
  usuariosMlab.get(urlusuariosMlab + queryName + apiKey, function (err, res, body) {
    var respuesta = body[0];
    if (undefined == respuesta) {
      var data = {
        idCliente: request.body.documento,
        documento: request.body.documento,
        nombre: request.body.nombre,
        apellido: request.body.apellido,
        telefono: request.body.telefono,
        usuario: request.body.documento,
        clave: request.body.clave
      };
      usuariosMlab.post(urlusuariosMlab + apiKey, data, function(err, res, body) {
        if(err)
          response.status(404).send({error: "Error al crear el usuario"});
        else
          response.send(body);
      });
    } else {
      response.status(404).send({error: "Ya existe un usuario con esos datos"});
    }
  })
});

/****************************************
 *         Consultar Usuario
 *****************************************/
app.get('/usuarios/:idUsuario',function(request, response) {
  console.log('ingreso metodo get getUser');
  const queryName = 'q={"documento":"' + request.params.idUsuario + '"}&';
  usuariosMlab.get(urlusuariosMlab + queryName + apiKey, function (err, res, body) {
  var respuesta = body[0];
  if (undefined == respuesta)
    response.status(404).send({error: "Usuario no Existe"});
  else
    response.send(respuesta);
  })
});

/****************************************
 *         Actualizar Usuario
 *****************************************/
app.put('/usuarios/:idUsuario', function(request, response) {
  console.log('ingreso metodo put updateUser');
  const queryName='q={"documento":"' + request.params.idUsuario + '"}&';
  var updateObject = '{"$set":' + JSON.stringify(request.body) + '}';
  usuariosMlab.put(urlusuariosMlab + queryName + apiKey, JSON.parse(updateObject), function(err, resM, body) {
    if (err)
      console.log(err);
    else
      response.send(body);
  });
});

/****************************************
*         Listado Movimientos
*****************************************/
app.get('/movimientos/:idUsuario',function(request, response) {
  console.log('ingreso metodo get getMovements');
  const queryName = 'q={"idCliente":"' + request.params.idUsuario + '"}&';
  movimientosMLab.get(urlmovimientosMlab + queryName + apiKey, function (err, res, body) {
    var respuesta = body;
    if (undefined == respuesta)
      response.status(404).send({error: "No hay movimientos"});
    else
      response.send(respuesta);
  })
});

/****************************************
 *         LOGIN
 *****************************************/
app.post('/login',function(request, response) {
  const queryName = 'q={"documento":"' + request.body.documento + '"}&';
  usuariosMlab.get(urlusuariosMlab + queryName + apiKey, function (err, res, body) {
    var respuesta = body[0];
    if (undefined != respuesta) {
      if (request.body.clave == respuesta.clave) {
        response.status(200).send({ idUser: respuesta.documento, nombre: respuesta.nombre });
      } else {
        response.status(404).send({ error: "Password incorrecto" });
      }
    } else {
      response.status(400).send({ error: "Usuario incorrecto" });
    }
  });
});
